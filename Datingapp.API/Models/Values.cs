namespace Datingapp.API.Models
{
    public class Value
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}